* Wed Oct 30 2024 David Abdurachmanov <davidlt@rivosinc.com> - 1.17.0-3
- Enable riscv64

* Sat Jul 20 2024 Fedora Release Engineering <releng@fedoraproject.org> - 1.17.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_41_Mass_Rebuild

* Thu Jun 27 2024 Benson Muite <benson_muite@emailplus.org> - 1.17.0-1
- Upgrade to new release bz2280815

* Sat Apr 27 2024 Benson Muite <benson_muite@emailplus.org> - 1.16.0-1
- Upgrade to new release bz2256073

* Sat Jan 27 2024 Fedora Release Engineering <releng@fedoraproject.org> - 1.15.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Sun Dec 24 2023 Benson Muite <benson_muite@emailplus.org> - 1.15.0-1
- Upgrade to new release

* Sun Dec 24 2023 Benson Muite <benson_muite@emailplus.org> - 1.14.1-5
- Add patch to separate C and C++ flags from yselkowitz

* Sat Jul 22 2023 Fedora Release Engineering <releng@fedoraproject.org> - 1.14.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_39_Mass_Rebuild

* Thu Jun 22 2023 Benson Muite <benson_muite@emailplus.org> - 1.14.1-3
- Apply fix to enable building with GCC13

* Fri Jun 02 2023 Benson Muite <benson_muite@emailplus.org> - 1.14.1-2
- Remove changes in patches that have been applied

* Fri Jun 02 2023 Kamal Heib <kheib@redhat.com> - 1.14.1-1
- Update to upstream release 1.14.1

* Sun Feb 19 2023 Benson Muite <benson_muite@emailplus.org> - 1.13.1-4
- List additional licenses used
- Remove unneeded ldconfig calls

* Sat Feb 18 2023 Benson Muite <benson_muite@emailplus.org> - 1.13.1-3
- Fix type declaration error
- Fix to enable use of GCC 13
- Use SPDX license identifier
- Fix build requires #RBZ2166925

* Sat Jan 21 2023 Fedora Release Engineering <releng@fedoraproject.org> - 1.13.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_38_Mass_Rebuild

* Sat Dec 24 2022 Benson Muite <benson_muite@emailplus.org> - 1.13.1-1
- Update to upstream release 1.13.0

* Wed Aug 03 2022 Michal Schmidt <mschmidt@redhat.com> - 1.13.0-3
- Update to upstream release 1.13.0
- Drop autogen.sh call. Upstream tarball does not have it anymore.

* Sat Jul 23 2022 Fedora Release Engineering <releng@fedoraproject.org> - 1.12.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_37_Mass_Rebuild

* Sat Jan 22 2022 Fedora Release Engineering <releng@fedoraproject.org> - 1.12.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Mon Dec 13 2021 Honggang Li <honli@redhat.com> - 1.12.0-1
- Bump version to 1.12.0

* Mon Nov 01 2021 Honggang Li <honli@redhat.com> - 1.11.2-1
- Bump version to 1.11.2

* Mon Aug 09 2021 Yurii Shestakov <yuriis@nvidia.com> 1.11.0-1
- Bump version to 1.11.0

* Fri Jul 23 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1.10.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Thu May 27 2021 Yurii Shestakov <yuriis@nvidia.com> 1.10.1-2
- Bump version to 1.10.1

* Mon Apr 26 2021 Yurii Shestakov <yuriis@nvidia.com> 1.10.1-rc1
- Bump version to 1.10.1-rc1

* Wed Jan 27 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1.9.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Wed Nov 11 2020 Yossi Itigin <yosefe@mellanox.com> 1.10.0-1
- Make the RPM relocatable

* Mon Nov  2 2020 Orion Poplawski <orion@nwra.com> - 1.9.0-1
- Update to 1.9.0

* Fri Oct 30 2020 Jeff Law <law@redhat.com> 1.8.1-5
- Adjust workaround for gcc-11 diagnostic to narrow its scope

* Thu Oct 29 2020 Jeff Law <law@redhat.com> 1.8.1-4
- Disable -Warray-bounds diagnostics for gcc-11

* Wed Jul 29 2020 Fedora Release Engineering <releng@fedoraproject.org> - 1.8.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Mon Jul 20 2020 Jeff Law <law@redhat.com> 1.8.1-2
- Fix broken configure files compromised by LTO

* Wed Jul 1 2020 Yossi Itigin <yosefe@mellanox.com> 1.8.1-1
- Bump version to 1.8.1

* Sun Sep 22 2019 Yossi Itigin <yosefe@mellanox.com> 1.8.0-1
- Bump version to 1.8.0

* Sun Mar 24 2019 Yossi Itigin <yosefe@mellanox.com> 1.7.0-1
- Bump version to 1.7.0

* Thu Jan 24 2019 Yossi Itigin <yosefe@mellanox.com> 1.6.0-1
- Add cma, knem, and xpmem sub-packages

* Tue Nov 20 2018 Yossi Itigin <yosefe@mellanox.com> 1.6.0-1
- Bump version to 1.6.0

* Tue Nov 6 2018 Andrey Maslennikov <andreyma@mellanox.com> 1.5.0-1
- Bump version to 1.5.0
- See NEWS for details

* Tue Oct 30 2018 Andrey Maslennikov <andreyma@mellanox.com> 1.4.0-1
- See NEWS for details

* Mon Aug 20 2018 Andrey Maslennikov <andreyma@mellanox.com> 1.3.1-1
- See NEWS for details

* Thu Aug 16 2018 Andrey Maslennikov <andreyma@mellanox.com> 1.3.0-1
- Explicitly set gcc-c++ as requirements

* Wed Mar 7 2018 Andrey Maslennikov <andreyma@mellanox.com> 1.3.0-1
- See NEWS for details

* Mon Aug 21 2017 Andrey Maslennikov <andreyma@mellanox.com> 1.2.1-1
- Spec file now complies with Fedora guidelines

* Mon Jul 3 2017 Andrey Maslennikov <andreyma@mellanox.com> 1.2.0-1
- Fedora package created

